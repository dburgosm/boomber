﻿using UnityEngine;
using System.Collections;

public class bubble : MonoBehaviour {

	public GameObject PFSparks;
	public float speed;

	void start()
	{
	
	}

	void LateUpdate () 
	{
		if (Input.GetMouseButtonDown(0)) {
			RaycastHit hitInfo = new RaycastHit();
			if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hitInfo))
			{
				
				if (hitInfo.collider.gameObject.tag.Equals ("bubble")) {
					hitInfo.collider.gameObject.SetActive (false);
					Init.Instance.incrementNumberOfBubblesDestroyed ();
					PFSparks.GetComponent<ParticleSystem> ().startColor = hitInfo.collider.gameObject.GetComponent <MeshRenderer> ().material.color;
					Instantiate (PFSparks, hitInfo.point, Quaternion.identity) ;

				}
			}
		} 


		if (transform.gameObject.activeSelf) {
			transform.position = new Vector3(transform.position.x,transform.position.y + (1.0F*speed) , transform.position.z);
			if (transform.position.y > 33.0F) {
				transform.gameObject.SetActive (false);
			}
		}
	}
}
