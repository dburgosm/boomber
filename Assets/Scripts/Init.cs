﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class Init : Singleton<Init> {

	public  GameObject PFBubble;
	public  int totalBubbles;
	public  double totalTimeInMilliSeconds;
	public  Text scoreText;
	public  Text leftTimeText; 
	private int numberOfBubblesCreated;
	private int numberOfBubblesDestroyed;
	private bool start = false;
	private System.DateTime startDateTime;
	private System.DateTime lastCreated ;
	private float bubblesCreateTime;
	private System.TimeSpan diffTime;
	private System.TimeSpan leftDateTime;
	private double leftTime;
	private List<Material> materials = new List<Material>();

	protected Init () {}

	// Use this for initialization
	void Start () {



		materials.Add (Resources.Load("Materials/orange", typeof(Material)) as Material);
		materials.Add (Resources.Load("Materials/yellow", typeof(Material)) as Material);
		materials.Add (Resources.Load("Materials/red", typeof(Material)) as Material);
		materials.Add (Resources.Load("Materials/green", typeof(Material)) as Material);

		bubblesCreateTime = (float)totalTimeInMilliSeconds/totalBubbles;
		startDateTime = System.DateTime.Now;
		numberOfBubblesDestroyed = 0;
		start = true;
	}
	
	// Update is called once per frame
	void Update () {
		if (start) {
			if (numberOfBubblesDestroyed < totalBubbles) {
				leftDateTime =  System.DateTime.Now - startDateTime;
				leftTimeText.text = leftDateTime.TotalSeconds.ToString();
				createNewBubbles ();
			}
		}
	}

	public void incrementNumberOfBubblesDestroyed(){
		numberOfBubblesDestroyed++;
		scoreText.text = numberOfBubblesDestroyed.ToString();
	}

	public void createNewBubbles(){
		int bubblesInGame = GameObject.FindGameObjectsWithTag("bubble").Length;

		if (bubblesInGame < (totalBubbles - numberOfBubblesDestroyed)) {
			diffTime = System.DateTime.Now - lastCreated;
			if (diffTime.TotalMilliseconds >= bubblesCreateTime) {
				GameObject gObject = Instantiate (PFBubble, new Vector3 (Random.Range (-6.0F, 6.0F), Random.Range (-7.0F, -10.0F), 1), Quaternion.identity) as GameObject;
				int randomMaterial = Random.Range (0, materials.Count);
				gObject.GetComponent <MeshRenderer> ().material = materials [randomMaterial];
				numberOfBubblesCreated++;
				lastCreated = System.DateTime.Now;
			}
		}
	}
}

